package elevatorspl.coreemptytwothirdsfullexecutivefloor.se.hh.ceres;

/*** added by dPerson
 */
public class Person {
	public double weight;
	public int destination;
	public boolean destinationReached = false;
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public int getDestination() {
		return destination;
	}
	public void setDestination(int destination) {
		this.destination = destination;
	}
	public boolean isDestinationReached() {
		return destinationReached;
	}
	public void setDestinationReached(boolean destinationReached) {
		this.destinationReached = destinationReached;
	}
	public void enterElevator(Elevator elevator) {
		elevator.pressInLiftFloorButton(destination);
	}
	public void leaveElevator() {
		destinationReached = true;
	}
}