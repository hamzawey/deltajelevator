package elevatorspl.coreexecutivefloor.se.hh.ceres;

/*** added by dElevator* modified by dExecutiveFloor
 */
public class Elevator {
	public int currentFloorID = 0;
	public boolean headingUp = true;
	public boolean doorsOpen = false;
	public Environment environment;
	public Person [] persons = new Person[5];
	public boolean [] floorButtons = new boolean[5];
	public Elevator(Environment environment) {
		this.environment = environment;
	}
	public int getCurrentFloorID() {
		return currentFloorID;
	}
	public void setCurrentFloorID(int currentFloorID) {
		this.currentFloorID = currentFloorID;
	}
	public boolean isBlocked() {
		return false;
	}
	public void enterElevator(Person person) {
		for(int i = 0;
			i < persons.length;
			i ++) {
			if(persons[i] == null) {
				persons[i] = person;
				break;
			}
		}
		person.enterElevator(this);
	}
	public boolean leaveElevator(Person person) {
		for(int i = 0;
			i < persons.length;
			i ++) {
			if(this.persons[i] == person) {
				this.persons[i] = null;
				return true;
			}
		}
		return false;
	}
	public void pressInLiftFloorButton(int floorID) {
		floorButtons[floorID] = true;
	}
	private void resetFloorButton(int floorID) {
		floorButtons[floorID] = false;
	}
	public boolean areDoorsOpen() {
		return doorsOpen;
	}
	public void timeShift() {
		if(stopRequestedAtCurrentFloor()) {
			doorsOpen = true;
			for(int i = 0;
				i < persons.length;
				i ++) {
				Person person = persons[i];
				if(person.getDestination() == currentFloorID) {
					leaveElevator(person);
				}
			}
			Floor floor = environment.getFloor(currentFloorID);
			floor.processWaitingPersons(this);
			resetFloorButton(currentFloorID);
		}
		else {
			if(doorsOpen) {
				doorsOpen = ! doorsOpen;
			}
			if(stopRequestedInDirection(headingUp, true, true)) {
				continueInDirection(headingUp);
			}
			else {
				if(stopRequestedInDirection(! headingUp, true, true)) {
					continueInDirection(! headingUp);
				}
				else {
					continueInDirection(headingUp);
				}
			}
		}
	}
	/*** modified by dExecutiveFloor
	 */
	private boolean stopRequestedAtCurrentFloor() {
		if(isExecutiveFloorCalling() && ! isExecutiveFloor(currentFloorID)) {
			return false;
		}
		else {
			return stopRequestedAtCurrentFloor_original2();
		}
	}
	private void continueInDirection(boolean headingUp) {
		this.headingUp = headingUp;
		if(headingUp) {
			if(environment.isTopFloor(currentFloorID)) {
				headingUp = ! headingUp;
			}
		}
		else {
			if(currentFloorID == 0) {
				headingUp = ! headingUp;
			}
		}
		if(headingUp) {
			currentFloorID = currentFloorID + 1;
		}
		else {
			currentFloorID = currentFloorID - 1;
		}
	}
	/*** modified by dExecutiveFloor
	 */
	private boolean stopRequestedInDirection(boolean headingUp, boolean
		respectFloorCalls, boolean respectInLiftCalls) {
		boolean stopRequestedInDirection;
		if(isExecutiveFloorCalling()) {
			boolean isStopRequestInDirection = currentFloorID < executiveFloor;
			boolean isHeadingUp = headingUp == this.headingUp;
			stopRequestedInDirection = isStopRequestInDirection == isHeadingUp;
		}
		else {
			stopRequestedInDirection = stopRequestedInDirection_original0(headingUp,
				respectFloorCalls, respectInLiftCalls);
		}
		return stopRequestedInDirection;
	}
	private boolean anyStopRequested() {
		Floor [] floors = environment.getFloors();
		for(int i = 0;
			i < floors.length;
			i ++) {
			Floor floor = floors[i];
			if(floor.hasCall() || floorButtons[i]) {
				return true;
			}
		}
		return false;
	}
	public boolean buttonForFloorIsPressed(int floorID) {
		return floorButtons[floorID];
	}
	public boolean getCurrentDirection() {
		return headingUp;
	}
	public Environment getEnvironment() {
		return environment;
	}
	public boolean isEmpty() {
		for(int i = 0;
			i < persons.length;
			i ++) {
			if(persons[i] != null) {
				return false;
			}
		}
		return true;
	}
	public boolean isIdle() {
		return ! anyStopRequested();
	}
	/*** added by dExecutiveFloor
	 */
	public int executiveFloor = 4;
	/*** added by dExecutiveFloor
	 */
	public boolean isExecutiveFloor(int floorID) {
		return floorID == executiveFloor;
	}
	/*** added by dExecutiveFloor
	 */
	public boolean isExecutiveFloorCalling() {
		Floor floors [] = environment.getFloors();
		for(int i = 0;
			i < floors.length;
			i ++) {
			Floor floor = floors[i];
			if(floor.getFloorID() == executiveFloor && floor.hasCall()) {
				return true;
			}
		}
		return false;
	}
	/*** modified by dExecutiveFloor
	 */
	private boolean stopRequestedInDirection_original0(boolean headingUp, boolean
		respectFloorCalls, boolean respectInLiftCalls) {
		boolean isFloorButtonPressed;
		boolean stopRequestInDirection;
		Floor floor;
		Floor [] floors = new Floor[5];
		floors = environment.getFloors();
		if(headingUp) {
			boolean isTopFloor = environment.isTopFloor(currentFloorID);
			if(isTopFloor) {
				stopRequestInDirection = false;
			}
			else {
				for(int i = currentFloorID + 1;
					i < floors.length;
					i ++) {
					floor = floors[i];
					if(respectFloorCalls && floor.hasCall()) {
						stopRequestInDirection = true;
					}
					else {
						isFloorButtonPressed = floorButtons[i];
						if(respectInLiftCalls && isFloorButtonPressed) {
							stopRequestInDirection = true;
						}
					}
				}
				stopRequestInDirection = false;
			}
		}
		else {
			if(currentFloorID == 0) {
				stopRequestInDirection = false;
			}
			else {
				for(int j = this.currentFloorID - 1;
					j >= 0;
					j --) {
					floor = floors[j];
					if(respectFloorCalls && floor.hasCall()) {
						stopRequestInDirection = true;
					}
					else {
						isFloorButtonPressed = floorButtons[j];
						if(respectInLiftCalls && isFloorButtonPressed) {
							stopRequestInDirection = true;
						}
					}
				}
			}
			stopRequestInDirection = false;
		}
		return stopRequestInDirection;
	}
	/*** modified by dExecutiveFloor
	 */
	private boolean stopRequestedAtCurrentFloor_original2() {
		Floor floor = environment.getFloor(currentFloorID);
		return floor.hasCall() || floorButtons[currentFloorID];
	}
}